<?php

const ALLOWED_ATTRIBUTES = [ 'id', 'class', 'href', 'src', 'alt' ];
const ALLOWED_TAGS       = [ 'h1', 'p', 'span', 'div', 'a', 'img' ];

function enclose_me( $content, $tag_name, $tag_attribute ) {
	if ( ! in_array( $tag_name, ALLOWED_TAGS ) ) {
		return ":/ tag '$tag_name' is not allowed";
	}
	$attribute_str = serialize_array( $tag_attribute, '=', '"', ' ' );
	$result        = "<$tag_name $attribute_str>$content</$tag_name>";

	return $result;
}

function serialize_array( $array, $connector, $encloser, $separator ) {
	$pairs = [];
	foreach ( $array as $key => $value ) {
		if ( in_array( $key, ALLOWED_ATTRIBUTES ) ) {
			array_push( $pairs, $key . $connector . $encloser . $value . $encloser );
		} else {
			echo ":/ attribute '$key' is not allowed";
		}
	}

	return implode( $separator, $pairs );
}


$sample_attributes_1 = [
	'id'    => 'header',
	'class' => 'col col-6 col-md-3',
	'style' => 'color:deeppink',
];

$sample_attributes_2 = [
	'src' => 'http://my-bad-script.com/script.js',
];


echo enclose_me( 'tom, you are awesome', 'h1', $sample_attributes_1 );
echo enclose_me( null, 'script', $sample_attributes_2 );