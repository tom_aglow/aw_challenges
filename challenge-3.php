<?php

function redirect_to($requested_url, $redirects) {
	foreach ( $redirects as $redirect ) {
		if ($redirect['source_url'] === $requested_url && $redirect['active_status']) {
			return $redirect['target_url'];
		}
	}
	return null;
}

// sample data
$redirect_a = array(
	'source_url' => 'http://project.wordpress.test/',
	'target_url' => 'https://www.vanarts.com',
	'active_status' => true
);
$redirect_b = array(
	'source_url' => 'http://project.wordpress.test/blog',
	'target_url' => 'https://www.vanarts.com/news/',
	'active_status' => false
);

$redirects = array($redirect_a, $redirect_b);

var_dump(redirect_to('http://project.wordpress.test/', $redirects));
var_dump(redirect_to('http://project.wordpress.test/blog', $redirects));